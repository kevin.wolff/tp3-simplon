# TP 3 - Site Simplon Grenoble

**Projet en binome avec [Mickael Vandenbulcke](https://gitlab.com/mickael.v)**

**A rendre pour le 03/07/2020 au soir**

[Lien vers la maquette](https://www.figma.com/file/hncl2KBiHsYkfCTFhN2LbR/Maquette-Simplonline?node-id=0%3A1)

### Consignes : 

Créer un mini site pour le centre de formation Simplon de Grenoble. Le site devra présenter la formation Simplon ainsi que donner les informations utiles sur la fabrique de Simplon.
Vous devez respecter la charte graphique du groupe Simplon (couleur, police d'écriture, etc ...). Vous devez aussi intégrer le logo du Simplon.
Aucune maquette graphique n'a été définit pour le site, vous avez carte blanche.
Le site devra être réalisé en HTML et CSS.

### Points obligatoires :

* realiser en HTML5, CSS3
* responsivité, respect des règles d'accessibilités et optimisé pour le SEO
* utiliser SASS pour la création de votre CSS
* vous devez utiliser une convention de nommage ( PascaleCase )
* le site doit contenir un formulaire de contact, non fonctionnel.

